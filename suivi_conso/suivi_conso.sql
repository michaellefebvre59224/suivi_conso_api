-- creation des tables

drop table if exists plein;
drop table if exists entretien;
drop table if exists vehicule;

create table vehicule(
	id_vehicule varchar(255) NOT NULL,
    id_utilisateur varchar(255),
	nom varchar(50) not null,
	marque varchar(50) not null,
	model varchar(50) not null,
	annee int(11),
	cylindree int(11),
	fiscal int(11),
	din int(11),
	transmission varchar(20),
	carburant varchar(20) not null,
	motrice varchar(20),
    immatriculation varchar(20),
constraint UK_VEHICULE_ID UNIQUE(id_vehicule),
constraint PK_VEHICULE_ID primary key (id_vehicule)
)engine=INNODB;

create table plein(
    id_plein varchar(255) NOT NULL,
    id_vehicule varchar(255) not null,
    first boolean,
    date_plein date not null,
    kilometrage_total int(11),
    kilometrage_journalier int(11),
    volume real,
    prix real,
    carburant varchar(10) not null,
constraint UK_PLEIN_ID UNIQUE(id_plein),
constraint PK_ADRESSE_ID_UTIL primary key (id_plein),
constraint FK_VEHICULE_PLEIN foreign key (id_vehicule) references vehicule(id_vehicule)
)engine=INNODB;


create table entretien(
  	id_entretien varchar(255) NOT NULL,
  	id_vehicule varchar(255) not null,
    date_entretien date not null,
  	kilometrage int(11),
  	prix real,
    detail varchar(255),
constraint UK_ENTRETIEN_ID UNIQUE(id_entretien),
constraint PK_ENTRETIEN_ID primary key (id_entretien),
constraint FK_VEHICULE_ENTRETIEN foreign key(id_vehicule) references vehicule(id_vehicule)
)engine=INNODB;
