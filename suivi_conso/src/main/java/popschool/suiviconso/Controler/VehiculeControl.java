package popschool.suiviconso.Controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.suiviconso.model.Vehicule;
import popschool.suiviconso.service.SuiviConsoService;

import java.util.List;

@RestController
public class VehiculeControl {

    @Autowired
    private SuiviConsoService suiviConsoService;

    //récupération de la liste des vehicules
    @CrossOrigin
    @GetMapping(value = "/vehicules")
    public ResponseEntity<List<Vehicule>> getAllVehicule(){
        try{
            List<Vehicule> vehicules = suiviConsoService.findAllVehicule();
            return ResponseEntity.status(HttpStatus.OK).body(vehicules);
        }catch (Exception ex){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @CrossOrigin
    @GetMapping(value = "/vehicules/{id}")
    public ResponseEntity<Vehicule> getVehiculeById(@PathVariable("id") String id){
        try{
            Vehicule vehicule = suiviConsoService.findVehiculeById(id);
            return ResponseEntity.status(HttpStatus.OK).body(vehicule);
        }catch (Exception ex){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @CrossOrigin
    @PostMapping(value = "/vehicules")
    public ResponseEntity<Vehicule> postCreateVehicule(@RequestBody Vehicule vehicule) {
        if (vehicule != null) {
            try {
                Vehicule responseVehicule = suiviConsoService.createVehicule(vehicule);
                ResponseEntity.status(HttpStatus.CREATED).body(responseVehicule);
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @CrossOrigin
    @PutMapping(value = "/vehicules/update/{id}")
    public ResponseEntity<Vehicule> patchUpdateVehicule(@RequestBody Vehicule vehicule,
            @PathVariable("id") String id){
        if (vehicule != null) {
            try {
                vehicule.setId(id);
                Vehicule responseVehicule = suiviConsoService.updateVehicule(vehicule);
                ResponseEntity.status(HttpStatus.CREATED).body(responseVehicule);
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @CrossOrigin
    @DeleteMapping(value = "/vehicules/delete/{id}")
    public ResponseEntity<Void> deleteVehicule(@PathVariable("id") String id){
        System.out.println("delete le vehicule : " + id);
        if(id !=null){
            try{
                suiviConsoService.deleteVehiculeById(id);
                return ResponseEntity.status(HttpStatus.OK).body(null);
            }catch (Exception ex){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
