package popschool.suiviconso.Controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.suiviconso.model.Plein;
import popschool.suiviconso.model.Vehicule;
import popschool.suiviconso.service.SuiviConsoService;

import java.util.List;

@RestController
public class PleinControl {

    @Autowired
    private SuiviConsoService suiviConsoService;

    //récupération de la liste de tous les pleins
    @GetMapping(value = "/pleins")
    public ResponseEntity<List<Plein>> getAllPlein(){
        try{
            List<Plein> pleins = suiviConsoService.findAllPlein();
            return ResponseEntity.status(HttpStatus.OK).body(pleins);
        }catch (Exception ex){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    //récupération de la liste des plein d'un véhicule
    @GetMapping(value = "/pleins/{vehicule}")
    public ResponseEntity<List<Plein>> getAllPleinByVehiculeOrderDate(@PathVariable("vehicule") Vehicule vehicule){
        try{
            List<Plein> pleins = suiviConsoService.findPleinByVehiculeOrderByDate(vehicule);
            return ResponseEntity.status(HttpStatus.OK).body(pleins);
        }catch (Exception ex){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(value = "/pleins/{id}")
    public ResponseEntity<Plein> getPleinById(@PathVariable("id") String id){
        try{
            Plein plein = suiviConsoService.findPleinById(id);
            return ResponseEntity.status(HttpStatus.OK).body(plein);
        }catch (Exception ex){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping(value = "/pleins")
    public ResponseEntity<Plein> postCreatePlein(@RequestBody Plein plein) {
        if (plein != null) {
            try {
                Plein responsePlein = suiviConsoService.createPlein(plein);
                ResponseEntity.status(HttpStatus.CREATED).body(responsePlein);
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @PutMapping(value = "/pleins/update/{id}")
    public ResponseEntity<Plein> patchUpdatePlein(@RequestBody Plein plein,
            @PathVariable("id") String id){
        if (plein != null) {
            try {
                plein.setId(id);
                Plein responsePlein = suiviConsoService.updatePlein(plein);
                ResponseEntity.status(HttpStatus.CREATED).body(responsePlein);
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @DeleteMapping(value = "/pleins/delete/{id}")
    public ResponseEntity<Void> deletePlein(@PathVariable("id") String id){
        if(id !=null){
            try{
                suiviConsoService.deletePleinById(id);
                return ResponseEntity.status(HttpStatus.OK).body(null);
            }catch (Exception ex){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping(value = "/pleins/consoMoyenne")
    public ResponseEntity<Double> getconsoMoyenneDuVehicule(@RequestBody Vehicule vehicule) {
        if (vehicule != null){
            try{
                Double consoMoyenne = suiviConsoService.getAverageConsoByVehicule(vehicule);
                return ResponseEntity.status(HttpStatus.OK).body(consoMoyenne);
            }catch (Exception ex){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping(value = "/pleins/consoPlein")
    public ResponseEntity<Double> getconsoDuPlein(@RequestBody Plein plein) {
        if (plein != null){
            try{
                Double conso = suiviConsoService.getConsoByPlein(plein);
                return ResponseEntity.status(HttpStatus.OK).body(conso);
            }catch (Exception ex){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping(value = "/pleins/coutAuCent" +
            "impreza_59" +
            "")
    public ResponseEntity<Double> getcoutAuCentDuVehicule(@RequestBody Vehicule vehicule) {
        if (vehicule != null){
            try{
                Double coutCent = suiviConsoService.getCoutAuCent(vehicule);
                return ResponseEntity.status(HttpStatus.OK).body(coutCent);
            }catch (Exception ex){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
