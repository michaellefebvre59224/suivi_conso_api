package popschool.suiviconso.Controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.suiviconso.model.Entretien;
import popschool.suiviconso.model.Plein;
import popschool.suiviconso.model.Vehicule;
import popschool.suiviconso.service.SuiviConsoService;

import java.util.List;

@RestController
public class EntretienControl {

    @Autowired
    private SuiviConsoService suiviConsoService;

    //récupération de la liste de tous les pleins
    @GetMapping(value = "/entetiens")
    public ResponseEntity<List<Entretien>> getAllEntretiens(){
        try{
            List<Entretien> entretiens = suiviConsoService.findAllEntretien();
            return ResponseEntity.status(HttpStatus.OK).body(entretiens);
        }catch (Exception ex){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    //récupération de la liste des plein d'un véhicule
    @GetMapping(value = "/entretiens/{vehicule}")
    public ResponseEntity<List<Entretien>> getAllEntretienByVehiculeOrderDate(@PathVariable("vehicule") Vehicule vehicule){
        try{
            List<Entretien> entretiens = suiviConsoService.findEntretienByVehiculeOrderByDate(vehicule);
            return ResponseEntity.status(HttpStatus.OK).body(entretiens);
        }catch (Exception ex){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(value = "/entretiens/{id}")
    public ResponseEntity<Entretien> getEntretienById(@PathVariable("id") String id){
        try{
            Entretien entretien = suiviConsoService.findEntetienById(id);
            return ResponseEntity.status(HttpStatus.OK).body(entretien);
        }catch (Exception ex){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping(value = "/entretiens")
    public ResponseEntity<Entretien> postCreateEntretien(@RequestBody Entretien entretien) {
        if (entretien != null) {
            try {
                Entretien responseEntretien = suiviConsoService.createEntretien(entretien);
                ResponseEntity.status(HttpStatus.CREATED).body(responseEntretien);
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @PutMapping(value = "/entretiens/update/{id}")
    public ResponseEntity<Plein> patchUpdatePlein(@RequestBody Entretien entretien,
            @PathVariable("id") String id){
        if (entretien != null && id!= null) {
            try {
                entretien.setId(id);
                Entretien responseEntretien = suiviConsoService.updateEntretien(entretien);
                ResponseEntity.status(HttpStatus.CREATED).body(responseEntretien);
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @DeleteMapping(value = "/entretiens/delete/{id}")
    public ResponseEntity<Void> deletePlein(@PathVariable("id") String id){
        if(id !=null){
            try{
                suiviConsoService.deleteEntretienById(id);
                return ResponseEntity.status(HttpStatus.OK).body(null);
            }catch (Exception ex){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping(value = "/entretiens/coutAuCent")
    public ResponseEntity<Double> getcoutAuCentDuVehicule(@RequestBody Vehicule vehicule) {
        if (vehicule != null){
            try{
                Double coutCent = suiviConsoService.getCoutAuCentEntretien(vehicule);
                return ResponseEntity.status(HttpStatus.OK).body(coutCent);
            }catch (Exception ex){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping(value = "/entretiens/coutTotal")
    public ResponseEntity<Double> getcoutTotalDuVehicule(@RequestBody Vehicule vehicule) {
        if (vehicule != null){
            try{
                Double coutTotal = suiviConsoService.getCoutTotalEntretien(vehicule);
                return ResponseEntity.status(HttpStatus.OK).body(coutTotal);
            }catch (Exception ex){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

}
