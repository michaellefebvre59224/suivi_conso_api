package popschool.suiviconso;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import popschool.suiviconso.model.Plein;
import popschool.suiviconso.model.Vehicule;
import popschool.suiviconso.service.SuiviConsoService;

import java.util.Date;
import java.util.List;

@SpringBootApplication
public class SuiviconsoApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SuiviconsoApplication.class, args);
    }


    private static final Logger log = LoggerFactory.getLogger(SuiviconsoApplication.class);

    @Autowired
    private SuiviConsoService suiviConsoService;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("//////////////////////////////////////////");
        System.out.println("//          RUN DE L'APP                //");
        System.out.println("//////////////////////////////////////////");
        Vehicule vehicule = suiviConsoService.findVehiculeById("uidtest2");
        System.out.println(vehicule.getNom());
        /*Date date = new Date();
        System.out.println("//////////date///////////");
        System.out.println(date);*/
        //Plein premierPlein = new Plein("plein1", date, 100000, 0, 30.00, 30.00, "ESSENCE", true, vehicule);
        //Plein plein = new Plein("plein3", date, 100600, 0, 30.00, 30.00, "ESSENCE", false, vehicule);
        //suiviConsoService.createPlein(plein);
        //List<Plein> pleins = suiviConsoService.findPleinByVehiculeOrderByDate(vehicule);
//        System.out.println(pleins);
        //int lenghtPleins = pleins.size();
        //System.out.println("nombre de pleins : " + lenghtPleins);
        /*for (int i=lenghtPleins-1; i>0; i--){
            int distance = pleins.get(i).getKilometrageTotal()- pleins.get(i-1).getKilometrageTotal();
            Double conso = pleins.get(i).getVolume()*100/distance;
            System.out.println("plein : ");
            System.out.println(pleins.get(i));
            System.out.println("distance : " + distance);
            System.out.println("conso : " + conso);
        }*/
        //suiviConsoService.getConsoByPlein(pleins.get(2));
        System.out.println("cout au cent du vehicule : ");
        System.out.println(suiviConsoService.getCoutAuCent(vehicule));
    }
}
