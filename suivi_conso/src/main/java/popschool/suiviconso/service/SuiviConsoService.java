package popschool.suiviconso.service;

import popschool.suiviconso.model.Entretien;
import popschool.suiviconso.model.Plein;
import popschool.suiviconso.model.Vehicule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SuiviConsoService {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                              VEHICULE
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    List<Vehicule> findAllVehicule();
    Vehicule findVehiculeById(String id);
    Vehicule createVehicule(Vehicule vehicule);
    Vehicule updateVehicule(Vehicule vehicule);
    void deleteVehicule(Vehicule vehicule);
    void deleteVehiculeById(String id);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                              PLEIN
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    List<Plein> findAllPlein();
    List<Plein> findPleinByVehiculeOrderByDate(Vehicule vehicule);
    Page<Plein> findAllPleinPageable(Pageable pageable);
    Plein findPleinById(String id);
    Plein createPlein(Plein plein);
    Plein updatePlein(Plein plein);
    void deletePlein(Plein plein);
    void deletePleinById(String id);
    Double getConsoByPlein(Plein plein);
    Double getAverageConsoByVehicule(Vehicule vehicule);
    void initConsoVehicule(Vehicule vehicule);
    Double getCoutAuCent(Vehicule vehicule);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                              ENTRETIEN
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    List<Entretien> findAllEntretien();
    List<Entretien> findEntretienByVehiculeOrderByDate(Vehicule vehicule);
    Page<Entretien> findAllEntretienPageable(Pageable pageable);
    Entretien findEntetienById(String id);
    Entretien createEntretien(Entretien entretien);
    Entretien updateEntretien(Entretien entretien);
    void deleteEntretien(Entretien entretien);
    void deleteEntretienById(String id);
    Double getCoutAuCentEntretien(Vehicule vehicule);
    Double getCoutTotalEntretien(Vehicule vehicule);


}
