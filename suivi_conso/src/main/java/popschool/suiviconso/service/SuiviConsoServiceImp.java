package popschool.suiviconso.service;

import popschool.suiviconso.dao.VehiculeRepo;
import popschool.suiviconso.dao.PleinRepo;
import popschool.suiviconso.dao.EntretienRepo;
import popschool.suiviconso.model.Entretien;
import popschool.suiviconso.model.Plein;
import popschool.suiviconso.model.Vehicule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class SuiviConsoServiceImp implements SuiviConsoService{

    @Autowired
    private VehiculeRepo vehiculeRepo;

    @Autowired
    private PleinRepo pleinRepo;

    @Autowired
    private EntretienRepo entretienRepo;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                              VEHICULE
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public List<Vehicule> findAllVehicule() {
        return vehiculeRepo.findAll();
    }

    @Override
    public Vehicule findVehiculeById(String id) {
        Optional<Vehicule> vehicule = vehiculeRepo.findById(id);
        if (vehicule.isPresent())return vehiculeRepo.findById(id).get();
        return null;
    }

    @Override
    public Vehicule createVehicule(Vehicule vehicule) {
        return vehiculeRepo.save(vehicule);
    }

    @Override
    public Vehicule updateVehicule(Vehicule vehicule) {
        return vehiculeRepo.save(vehicule);
    }

    @Override
    public void deleteVehicule(Vehicule vehicule) {
        vehiculeRepo.delete(vehicule);
    }

    @Override
    public void deleteVehiculeById(String id) {
        try{
            vehiculeRepo.deleteById(id);
        }catch (Exception ex){
            System.out.println("Exception delete");
            System.out.println(ex);
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                              PLEIN
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public List<Plein> findAllPlein() {
        return pleinRepo.findAll();
    }

    @Override
    public List<Plein> findPleinByVehiculeOrderByDate(Vehicule vehicule) {
        return pleinRepo.findByVehiculeOrderByDate(vehicule);
    }

    @Override
    public Page<Plein> findAllPleinPageable(Pageable pageable) {
        return pleinRepo.findAll(pageable);
    }

    @Override
    public Plein findPleinById(String id) {
        Optional<Plein> plein = pleinRepo.findById(id);
        if (plein.isPresent())return plein.get();
        return null;
    }

    @Override
    public Plein createPlein(Plein plein) {
        return pleinRepo.save(plein);
    }

    @Override
    public Plein updatePlein(Plein plein) {
        return pleinRepo.save(plein);
    }

    @Override
    public void deletePlein(Plein plein) {
        pleinRepo.delete(plein);
    }

    @Override
    public void deletePleinById(String id) {
        pleinRepo.deleteById(id);
    }

    @Override
    public Double getConsoByPlein(Plein plein) {
        List<Plein> pleins = pleinRepo.findByVehiculeOrderByDate(plein.getVehicule());
        Double conso = 0.00;

        if (plein.getFirst() == false){
            for (int i = 0; i < pleins.size(); i++){
                if (pleins.get(i).equals(plein)){
                    Plein pleinPrecedent = pleins.get(i-1);
                    plein.setKilometrageJournalier(plein.getKilometrageTotal()-pleinPrecedent.getKilometrageTotal());
                    conso = plein.getVolume()*100/plein.getKilometrageJournalier();
                    // TODO inserer la conso dans la base sql
                }
            }
        }

        return conso;
    }

    @Override
    public Double getAverageConsoByVehicule(Vehicule vehicule) {
        List<Plein> pleins = pleinRepo.findByVehiculeOrderByDate(vehicule);
        int size = pleins.size();
        int distanceTotal = pleins.get(size-1).getKilometrageTotal() - pleins.get(0).getKilometrageTotal();
        Double volumeTotal = pleinRepo.findTotalVolume(vehicule);
        Double consoMoyenne = volumeTotal*100/distanceTotal;
        return consoMoyenne;
    }

    @Override
    public void initConsoVehicule(Vehicule vehicule) {
        List<Plein> pleins = pleinRepo.findByVehiculeOrderByDate(vehicule);

        pleins.forEach(plein -> {
            if (plein.getFirst() == false){
                for (int i = 0; i < pleins.size(); i++){
                    if (pleins.get(i).equals(plein)){
                        Plein pleinPrecedent = pleins.get(i-1);
                        plein.setKilometrageJournalier(plein.getKilometrageTotal()-pleinPrecedent.getKilometrageTotal());
                        Double conso = plein.getVolume()*100/plein.getKilometrageJournalier();
                        // TODO inserer la conso dans la base sql
                    }
                }
            }
        });
    }

    @Override
    public Double getCoutAuCent(Vehicule vehicule){
        List<Plein> pleins = pleinRepo.findByVehiculeOrderByDate(vehicule);
        int size = pleins.size();
        int distanceTotal = pleins.get(size-1).getKilometrageTotal() - pleins.get(0).getKilometrageTotal();
        Double coutTotal = pleinRepo.findTotalCout(vehicule);
        Double prixMoyen = coutTotal*100/distanceTotal;
        return prixMoyen;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                              ENTRETIEN
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public List<Entretien> findAllEntretien() {
        return entretienRepo.findAll();
    }

    @Override
    public Page<Entretien> findAllEntretienPageable(Pageable pageable) {
        return entretienRepo.findAll(pageable);
    }

    @Override
    public List<Entretien> findEntretienByVehiculeOrderByDate(Vehicule vehicule) {
        return entretienRepo.findByVehiculeOrderByDate(vehicule);
    }

    @Override
    public Entretien findEntetienById(String id) {
        Optional<Entretien> entretien = entretienRepo.findById(id);
        if (entretien.isPresent())return entretien.get();
        return null;
    }

    @Override
    public Entretien createEntretien(Entretien entretien) {
        return entretienRepo.save(entretien);
    }

    @Override
    public Entretien updateEntretien(Entretien entretien) {
        return entretienRepo.save(entretien);
    }

    @Override
    public void deleteEntretien(Entretien entretien) {
        entretienRepo.delete(entretien);
    }

    @Override
    public void deleteEntretienById(String id) {
        entretienRepo.deleteById(id);
    }

    @Override
    public Double getCoutAuCentEntretien(Vehicule vehicule){
        List<Entretien> entretiens = entretienRepo.findByVehiculeOrderByDate(vehicule);
        int size = entretiens.size();
        int distanceTotal = entretiens.get(size-1).getKilometrage() - entretiens.get(0).getKilometrage();
        Double coutTotal = entretienRepo.findTotalCoutEntretien(vehicule);
        Double prixMoyen = coutTotal*100/distanceTotal;
        return prixMoyen;
    }

    @Override
    public Double getCoutTotalEntretien(Vehicule vehicule){
        Double coutTotal = entretienRepo.findTotalCoutEntretien(vehicule);
        return  coutTotal;
    }

}
