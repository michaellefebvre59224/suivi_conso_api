package popschool.suiviconso.dao;

import org.springframework.data.jpa.repository.Query;
import popschool.suiviconso.model.Plein;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import popschool.suiviconso.model.Vehicule;

import java.util.List;

public interface PleinRepo extends JpaRepository<Plein, String> {

    Page<Plein> findAll(Pageable pageable);
    List<Plein> findAll();
    List<Plein> findByVehiculeOrderByDate(Vehicule vehicule);

    //Calcul du volume total de carburant
    @Query(value = "SELECT SUM(p.volume) FROM Plein p WHERE p.vehicule = ?1 and p.first = false")
    Double findTotalVolume(Vehicule vehicule);

    //Calcul du cout total en carburant
    @Query(value = "SELECT SUM(p.prix) FROM Plein p WHERE p.vehicule = ?1 and p.first = false")
    Double findTotalCout(Vehicule vehicule);
}
