package popschool.suiviconso.dao;

import org.springframework.data.jpa.repository.Query;
import popschool.suiviconso.model.Entretien;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import popschool.suiviconso.model.Plein;
import popschool.suiviconso.model.Vehicule;

import java.util.List;

public interface EntretienRepo extends JpaRepository<Entretien, String> {

    Page<Entretien> findAll(Pageable pageable);
    List<Entretien> findAll();
    List<Entretien> findByVehiculeOrderByDate(Vehicule vehicule);

    //Calcul du cout total en carburant
    @Query(value = "SELECT SUM(e.prix) FROM Entretien e WHERE e.vehicule = ?1")
    Double findTotalCoutEntretien(Vehicule vehicule);

}
