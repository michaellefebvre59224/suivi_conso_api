package popschool.suiviconso.dao;

import popschool.suiviconso.model.Vehicule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VehiculeRepo extends JpaRepository<Vehicule, String> {

    List<Vehicule> findAll();

}
