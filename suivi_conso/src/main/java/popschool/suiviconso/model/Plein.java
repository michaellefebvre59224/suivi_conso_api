package popschool.suiviconso.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@Table(name = "plein")
public class Plein {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      VARIABLE D'INSTANCE                                                       //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Id
    @Column(name = "id_plein")
    private String id;

    @Column(name = "date_plein")
    private Date date;

    @Column(name = "kilometrage_total")
    private int kilometrageTotal;

    @Column(name = "kilometrage_journalier")
    private int kilometrageJournalier;

    private Double volume;

    private Double prix;

    //  type de carburant mis lors du plein
    private String carburant;

    //identifie le premier plein afin d'initialisé la conso
    private Boolean first;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      RELATION
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @ManyToOne
    @JoinColumn(name = "id_vehicule")
    private Vehicule vehicule;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      CONSTRUCTOR
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public Plein(String id, Date date, int kilometrageTotal, int kilometrageJournalier, Double volume, Double prix, String carburant, Boolean first, Vehicule vehicule) {
        this.id = id;
        this.date = date;
        this.kilometrageTotal = kilometrageTotal;
        this.kilometrageJournalier = kilometrageJournalier;
        this.volume = volume;
        this.prix = prix;
        this.carburant = carburant;
        this.first = first;
        this.vehicule = vehicule;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      TO STRING
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public String toString() {
        return "Plein{" +
                "id='" + id + '\'' +
                ", vehicule= " + vehicule.getNom() +
                ", date=" + date +
                ", kilometrageTotal=" + kilometrageTotal +
                ", kilometrageJournalier=" + kilometrageJournalier +
                ", volume=" + volume +
                ", prix=" + prix +
                ", carburant='" + carburant +
                ", first=" + first + '\'' +
                '}';
    }
}
