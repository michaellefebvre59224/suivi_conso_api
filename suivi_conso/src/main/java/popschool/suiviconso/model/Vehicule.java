package popschool.suiviconso.model;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "vehicule")
public class Vehicule {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      VARIABLE D'INSTANCE                                                       //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Id
    @Column(name = "id_vehicule")
    private String id;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // TODO     ADD FOR LINK CARS TO USER
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //private String idUtilisateur;

    //  permet à l'utilisateur d'edentifier son vehicule
    private String nom;

    private String marque;

    private String model;

    private int annee;

    private int cylindree;

    //  puissance fiscal du véhicule
    private int fiscal;

    //  puissance réel du véhicule en cv
    private int din;

    //  Boite a vitesse manuel ou automatique
    private String transmission;

    //  type de carburant
    private String carburant;

    //  type de transmission (traction, propulsion ou AWD)
    private String motrice;

    private String immatriculation;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      RELATION                                                                  //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @OneToMany(mappedBy = "vehicule")
    @JsonIgnore
    private List<Plein> pleins;

    @OneToMany(mappedBy = "vehicule")
    @JsonIgnore
    private List<Entretien> entretiens;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      CONSTRUCTEUR                                                              //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public Vehicule(String uid, String nom, String marque, String model, int annee, int cylindree, int cvFiscal, int cvDin, String transmision, String carburant, String motrice, String immatriculation) {
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      TO STRING ET HASH                                                         //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public String toString() {
        return "Vehicule{" +
                "id='" + id + '\'' +
                ", nom='" + nom + '\'' +
                ", marque='" + marque + '\'' +
                ", model='" + model + '\'' +
                ", annee=" + annee +
                ", cylindree=" + cylindree +
                ", fiscal=" + fiscal +
                ", din=" + din +
                ", transmission='" + transmission + '\'' +
                ", carburant='" + carburant + '\'' +
                ", motrice='" + motrice + '\'' +
                ", immatriculation='" + immatriculation + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicule)) return false;
        Vehicule vehicule = (Vehicule) o;
        return annee == vehicule.annee &&
                cylindree == vehicule.cylindree &&
                fiscal == vehicule.fiscal &&
                din == vehicule.din &&
                id.equals(vehicule.id) &&
                nom.equals(vehicule.nom) &&
                marque.equals(vehicule.marque) &&
                model.equals(vehicule.model) &&
                transmission.equals(vehicule.transmission) &&
                carburant.equals(vehicule.carburant) &&
                motrice.equals(vehicule.motrice) &&
                immatriculation.equals(vehicule.immatriculation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, marque, model, annee, cylindree, fiscal, din, transmission, carburant, motrice, immatriculation);
    }
}
