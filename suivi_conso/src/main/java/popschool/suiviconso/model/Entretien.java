package popschool.suiviconso.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "entretien")
public class Entretien {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      VARIABLE D'INSTANCE                                                       //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Id
    @Column(name = "id_entretien")
    private String id;

    @Column(name = "date_entetien")
    private Date date;

    private int kilometrage;

    private Double prix;

    //  permet de noter le detail de l'entretien effectué
    private String detail;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      RELATION
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @ManyToOne
    @JoinColumn(name = "id_vehicule")
    private Vehicule vehicule;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                      TO STRING
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    public String toString() {
        return "Entretien{" +
                "id='" + id + '\'' +
                ", date=" + date +
                ", kilometrage=" + kilometrage +
                ", prix=" + prix +
                ", detail='" + detail + '\'' +
                '}';
    }
}
